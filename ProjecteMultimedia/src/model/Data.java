package model;

import controller.pTSM;
/**
 * Copyrights
 * Project name: GerVit Codec
 * @author Vitaliy Konovalov
 * @author Gerard Rodriguez
 * University of Barcelona, 2012-2013
 */
import java.util.ArrayList;

/**
 * class Data 
 */
public class Data {
    private String PATH_SAVE_cGZIP = "../video.grv";// "../compressed_images.gzip";
    private String PATH_SAVE_GZIP = "../jpg_images.gzip";// "../compressed_images.gzip";
    private String INPUT_IMAGES_PATH = "../images/";//../images/";
    private int NB_OF_IMAGES = 99;
    private int NB_OF_IMAGES_PER_SECOND = 25;
    private int WIDTH = 300;
    private int HEIGHT = 300;
    private String filter = "null";
    private ArrayList<pTSM> images;

    public String getPATH_SAVE_cGZIP() {
        return PATH_SAVE_cGZIP;
    }

    public void setPATH_SAVE_cGZIP(String PATH_SAVE_cGZIP) {
        this.PATH_SAVE_cGZIP = PATH_SAVE_cGZIP;
    }

    public String getPATH_SAVE_GZIP() {
        return PATH_SAVE_GZIP;
    }

    public void setPATH_SAVE_GZIP(String PATH_SAVE_GZIP) {
        this.PATH_SAVE_GZIP = PATH_SAVE_GZIP;
    }

    public String getINPUT_IMAGES_PATH() {
        return INPUT_IMAGES_PATH;
    }

    public void setINPUT_IMAGES_PATH(String INPUT_IMAGES_PATH) {
        this.INPUT_IMAGES_PATH = INPUT_IMAGES_PATH;
    }

    public int getNB_OF_IMAGES() {
        return NB_OF_IMAGES;
    }

    public void setNB_OF_IMAGES(int NB_OF_IMAGES) {
        this.NB_OF_IMAGES = NB_OF_IMAGES;
    }

    public int getNB_OF_IMAGES_PER_SECOND() {
        return NB_OF_IMAGES_PER_SECOND;
    }

    public void setNB_OF_IMAGES_PER_SECOND(int NB_OF_IMAGES_PER_SECOND) {
        this.NB_OF_IMAGES_PER_SECOND = NB_OF_IMAGES_PER_SECOND;
    }

    public int getWIDTH() {
        return WIDTH;
    }

    public void setWIDTH(int WIDTH) {
        this.WIDTH = WIDTH;
    }

    public int getHEIGHT() {
        return HEIGHT;
    }

    public void setHEIGHT(int HEIGHT) {
        this.HEIGHT = HEIGHT;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public ArrayList<pTSM> getImages() {
        return images;
    }

    public void setImages(ArrayList<pTSM> images) {
        this.images = images;
    }

    public void add(pTSM im) {
        images.add(im);
    }

     

    
}
