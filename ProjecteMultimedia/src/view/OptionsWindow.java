/**
 * Copyrights Project name: GerVit Codec
 *
 * @author Vitaliy Konovalov
 * @author Gerard Rodriguez University of Barcelona, 2012-2013
 */
package view;

import controller.Codec;
import controller.Video;
import controller.VideoComprimit;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import model.Data;

/**
 * class OptionsWindow
 */
public class OptionsWindow extends javax.swing.JFrame {

    int tesSize;
    float threshold;
    float spiralTh;
    Data d = new Data();
    MainWindow mw;
    int FPS = 25;
    VideoComprimit glb_vidCompr = null;

    /**
     * Creates new form OptionsWindow
     */
    public OptionsWindow(MainWindow mw) {
        this.mw = mw;

        initComponents();
    }

    Data getData() {
        return d;
    }

    int getSliderB() {
        return sld_binari.getValue();
    }

    int getSliderE() {
        return sld_bright.getValue();
    }

    int getFPS() {
        return FPS;
    }

    public VideoComprimit getGlb_vidCompr() {
        return glb_vidCompr;
    }

    public void setGlb_vidCompr(VideoComprimit glb_vidCompr) {
        this.glb_vidCompr = glb_vidCompr;
    }

    /**
     * S'agafen els valors de mida de teseles i threshold dels elements de la
     * GUI
     *
     * @return 0 si tot correcte. -1 si no esta en rang permes
     */
    private int getDCTparameters() {

        // mida teseles
        switch (cbox_sizetesela.getSelectedIndex()) {
            case 0:
                tesSize = 8;
                break;
            case 1:
                tesSize = 16;
                break;
            case 2:
                tesSize = 32;
                break;

            case 3:
                tesSize = 52;
                break;

        }

        //threshold spiral : percentatge que es fa el recorregut
        String t = txf_spiral.getText();
        spiralTh = Float.parseFloat(t);
        if (spiralTh < 0 || spiralTh > 100) {
            JOptionPane.showMessageDialog(this, "Please enter value in range 0-100", "Value range error",
                    JOptionPane.WARNING_MESSAGE);
            return -1;
        }

        // threshold de diferencia entre pixels
        threshold = Float.parseFloat(txf_threshold.getText());
        if (threshold < 0 || threshold > 50) {
            JOptionPane.showMessageDialog(this, "Please enter value in range 0-50", "Value range error",
                    JOptionPane.WARNING_MESSAGE);
            return -1;
        }

        System.out.println(tesSize + " >< " + threshold);
        return 0;

    }

    void cleanStats() {
        txt_size.setText("");
        txt_seconds.setText("");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grp_tesela = new javax.swing.ButtonGroup();
        grp_filters = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        btn_readGzip = new javax.swing.JButton();
        btn_compress = new javax.swing.JButton();
        btn_saveVideo = new javax.swing.JButton();
        btn_loadNormal = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        txt_fps = new javax.swing.JTextField();
        lbl_fps = new javax.swing.JLabel();
        sld_fps = new javax.swing.JSlider();
        jPanel4 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        cbox_sizetesela = new javax.swing.JComboBox();
        txf_threshold = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txf_spiral = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txt_seconds = new javax.swing.JTextField();
        txt_size = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        sld_bright = new javax.swing.JSlider();
        sld_binari = new javax.swing.JSlider();
        rb_binari = new javax.swing.JRadioButton();
        rb_bright = new javax.swing.JRadioButton();

        setBounds(new java.awt.Rectangle(420, 22, 0, 0));

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("IO"));

        btn_readGzip.setText("Load Compressed");
        btn_readGzip.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_readGzipActionPerformed(evt);
            }
        });

        btn_compress.setText("Compress");
        btn_compress.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_compressActionPerformed(evt);
            }
        });

        btn_saveVideo.setText("Save Video");
        btn_saveVideo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_saveVideoActionPerformed(evt);
            }
        });

        btn_loadNormal.setText("Load Normal");
        btn_loadNormal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_loadNormalActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(btn_compress, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(btn_saveVideo, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 113, Short.MAX_VALUE)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(btn_readGzip, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 138, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btn_loadNormal, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .add(29, 29, 29))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(btn_compress, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 24, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btn_readGzip, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 29, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(btn_saveVideo)
                    .add(btn_loadNormal))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("General Configuration"));

        txt_fps.setText("25");
        txt_fps.setFocusable(false);

        lbl_fps.setText("FPS");

        sld_fps.setMaximum(35);
        sld_fps.setMinimum(10);
        sld_fps.setMinorTickSpacing(5);
        sld_fps.setPaintLabels(true);
        sld_fps.setPaintTicks(true);
        sld_fps.setSnapToTicks(true);
        sld_fps.setValue(25);
        sld_fps.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                sld_fpsMouseReleased(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel3Layout = new org.jdesktop.layout.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .add(lbl_fps)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(txt_fps, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(sld_fps, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel3Layout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(txt_fps, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(lbl_fps))
                    .add(sld_fps, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("DCT parameters"));

        jLabel5.setText("Tesela");

        cbox_sizetesela.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "8x8", "16x16", "32x32", "52x52" }));
        cbox_sizetesela.setSelectedIndex(2);
        cbox_sizetesela.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbox_sizeteselaActionPerformed(evt);
            }
        });

        txf_threshold.setText("2");
        txf_threshold.setToolTipText("0-50 ");

        jLabel1.setText("Threshold");
        jLabel1.setToolTipText("diferencia entre dos pixels");

        jLabel2.setText("%");

        txf_spiral.setText("12.5");

        jLabel3.setText("Threshold Spiral");

        org.jdesktop.layout.GroupLayout jPanel4Layout = new org.jdesktop.layout.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel4Layout.createSequentialGroup()
                        .add(jLabel5)
                        .add(84, 84, 84))
                    .add(jPanel4Layout.createSequentialGroup()
                        .add(6, 6, 6)
                        .add(cbox_sizetesela, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 64, Short.MAX_VALUE)))
                .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(jPanel4Layout.createSequentialGroup()
                        .add(18, 18, 18)
                        .add(txf_spiral, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 52, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jLabel2)
                        .add(121, 121, 121)
                        .add(txf_threshold, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 40, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(jPanel4Layout.createSequentialGroup()
                        .add(jLabel3)
                        .add(81, 81, 81)
                        .add(jLabel1)))
                .addContainerGap(18, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel4Layout.createSequentialGroup()
                .add(7, 7, 7)
                .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel5)
                    .add(jLabel1)
                    .add(jLabel3))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(cbox_sizetesela, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(txf_spiral, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel2)
                    .add(txf_threshold, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Statistics", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, null, new java.awt.Color(51, 0, 102)));

        jLabel4.setText("Elapsed time:");

        jLabel6.setText("Size: ");

        txt_seconds.setEditable(false);

        txt_size.setEditable(false);

        jLabel7.setText("seconds");

        jLabel8.setText("KB");

        org.jdesktop.layout.GroupLayout jPanel5Layout = new org.jdesktop.layout.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jLabel6)
                    .add(jLabel4))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(txt_size, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 60, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(txt_seconds, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 60, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel7)
                    .add(jLabel8))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel4)
                    .add(txt_seconds, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel7))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel6)
                    .add(txt_size, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel8))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder("Filters"));

        sld_bright.setMaximum(50);
        sld_bright.setMinimum(1);

        sld_binari.setMaximum(255);

        grp_filters.add(rb_binari);
        rb_binari.setText("Binarization");
        rb_binari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rb_binariActionPerformed(evt);
            }
        });

        grp_filters.add(rb_bright);
        rb_bright.setText("Brightness");
        rb_bright.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rb_brightActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel6Layout = new org.jdesktop.layout.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, rb_bright, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 111, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(rb_binari))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(sld_binari, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE)
                    .add(sld_bright, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel6Layout.createSequentialGroup()
                .addContainerGap(12, Short.MAX_VALUE)
                .add(jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel6Layout.createSequentialGroup()
                        .add(sld_binari, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(sld_bright, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel6Layout.createSequentialGroup()
                        .add(rb_binari)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(rb_bright)
                        .addContainerGap())))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(9, 9, 9)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jPanel3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jPanel4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(layout.createSequentialGroup()
                        .add(6, 6, 6)
                        .add(jPanel6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
            .add(layout.createSequentialGroup()
                .add(105, 105, 105)
                .add(jPanel5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(20, 20, 20)
                .add(jPanel3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(jPanel4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void rb_brightActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rb_brightActionPerformed
        // TODO add your handling code here:
        if (d.getFilter().equals("enfosqueix")) {
            d.setFilter("null");
        } else {
            d.setFilter("enfosqueix");
        }
        System.out.println(d.getFilter());
    }//GEN-LAST:event_rb_brightActionPerformed

    private void rb_binariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rb_binariActionPerformed
        // TODO add your handling code here:
        if (d.getFilter().equals("binaritza")) {
            d.setFilter("null");
        } else {
            d.setFilter("binaritza");
        }
        System.out.println(d.getFilter());
    }//GEN-LAST:event_rb_binariActionPerformed

    private void btn_readGzipActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_readGzipActionPerformed
        String path;
        JFileChooser fc = new JFileChooser();
        fc.setFileFilter(new FileNameExtensionFilter("GRV", "grv"));//filtrem nomes arxius zip
        File f = new java.io.File("../");
        fc.setCurrentDirectory(f);

        int returnVal = fc.showDialog(null, "Select compressed video");
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            path = fc.getSelectedFile().getParent() + "/" + fc.getSelectedFile().getName();
            if (fc.accept(f)) {
                mw.zipr.readGzipCompressed(path);
                mw.setLoaded(true);
            }
        } else if (returnVal == JFileChooser.ERROR_OPTION) {
            JOptionPane.showMessageDialog(null, "Error selecting file", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "File selection canceled", "Cancel", JOptionPane.WARNING_MESSAGE);
        }

    }//GEN-LAST:event_btn_readGzipActionPerformed

    private void btn_compressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_compressActionPerformed
        if (getDCTparameters() >= 0) {
            Codec codec = new Codec();
            Video video = new Video(d.getImages());
            double a = System.nanoTime();
            glb_vidCompr = codec.compressioDCThard(video, tesSize, threshold, spiralTh);
            double b = System.nanoTime();
            String temps = Double.toString((b - a));
            temps = temps.substring(0, 5);
            txt_seconds.setText(temps);
            
        }


    }//GEN-LAST:event_btn_compressActionPerformed

    private void sld_fpsMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sld_fpsMouseReleased
        System.out.println("Moving");
        FPS = sld_fps.getValue();
        txt_fps.setText(Integer.toString(FPS));        // TODO add your handling code here:
        mw.setFps(FPS);
    }//GEN-LAST:event_sld_fpsMouseReleased

    private void btn_loadNormalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_loadNormalActionPerformed
        String path;
        JFileChooser fc = new JFileChooser();
        fc.setFileFilter(new FileNameExtensionFilter("GZIP", "gzip"));//filtrem nomes arxius zip
        File f = new java.io.File("../");
        fc.setCurrentDirectory(f);

        int returnVal = fc.showDialog(null, "Select uncompressed video");
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            path = fc.getSelectedFile().getParent() + "/" + fc.getSelectedFile().getName();
            if (fc.accept(f)) {
                mw.zipr.readGzip(path);
                mw.setLoaded(true);
            }
        } else if (returnVal == JFileChooser.ERROR_OPTION) {
            JOptionPane.showMessageDialog(null, "Error selecting file", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "File selection canceled", "Cancel", JOptionPane.WARNING_MESSAGE);
        }


    }//GEN-LAST:event_btn_loadNormalActionPerformed

    private void btn_saveVideoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_saveVideoActionPerformed
        JFileChooser fileChooser = new JFileChooser();

        fileChooser.setCurrentDirectory(new java.io.File("../"));
        if (glb_vidCompr != null) {
            fileChooser.setFileFilter(new FileNameExtensionFilter("GRV", "grv"));
        } else {
            fileChooser.setFileFilter(new FileNameExtensionFilter("GZIP", "gzip"));
        }
        fileChooser.showSaveDialog(this);
        File tmp = fileChooser.getSelectedFile();
        if (tmp == null) {
            JOptionPane.showMessageDialog(this, "Saving file was aborted", "Warning", JOptionPane.WARNING_MESSAGE);
        } else {
            String path = tmp.getPath();

            if (glb_vidCompr == null) { //si s'ha comprimit previament
                path += ".gzip";
                mw.zipr.saveGzip(d.getImages(), path);
                File file = new File(path);
                String size = Double.toString(file.length() / 1024);
                txt_size.setText(size);
            } else {
                path += ".grv";
                mw.zipr.saveGzipCompressed(glb_vidCompr, path, tesSize);
                File file = new File(path);
                String size = Double.toString(file.length() / 1024);
                txt_size.setText(size);
            }

        }

    }//GEN-LAST:event_btn_saveVideoActionPerformed

    private void cbox_sizeteselaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbox_sizeteselaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbox_sizeteselaActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_compress;
    private javax.swing.JButton btn_loadNormal;
    private javax.swing.JButton btn_readGzip;
    private javax.swing.JButton btn_saveVideo;
    private javax.swing.JComboBox cbox_sizetesela;
    private javax.swing.ButtonGroup grp_filters;
    private javax.swing.ButtonGroup grp_tesela;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JLabel lbl_fps;
    private javax.swing.JRadioButton rb_binari;
    private javax.swing.JRadioButton rb_bright;
    private javax.swing.JSlider sld_binari;
    private javax.swing.JSlider sld_bright;
    private javax.swing.JSlider sld_fps;
    private javax.swing.JTextField txf_spiral;
    private javax.swing.JTextField txf_threshold;
    private javax.swing.JTextField txt_fps;
    private javax.swing.JTextField txt_seconds;
    private javax.swing.JTextField txt_size;
    // End of variables declaration//GEN-END:variables
}
