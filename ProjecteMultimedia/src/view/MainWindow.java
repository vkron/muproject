/**
 * Copyrights Project name: GerVit Codec
 *
 * @author Vitaliy Konovalov
 * @author Gerard Rodriguez University of Barcelona, 2012-2013
 */
package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import controller.Filter;
import controller.ZipGzip;
import java.awt.Color;
import javax.swing.Box.Filler;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;
import model.Data;

/**
 *
 * @author vito
 */
public class MainWindow extends javax.swing.JFrame {

    private OptionsWindow optionsFrame;
    Data dates;
    int global_i = 0;
    ZipGzip zipr;
    Filter f = new Filter();
    Timer t;
    boolean loaded = false;

    /**
     * Creates new form MainWindow
     */
    public MainWindow() {

        optionsFrame = new OptionsWindow(this);
        dates = optionsFrame.getData();
        zipr = new ZipGzip(dates);
        initComponents();
        txt_pathToZip.setEnabled(false);

        //btn_readGzip.setEnabled(false);

        int fps = optionsFrame.getFPS();
        System.out.println("fps::" + fps);
        t = new Timer(1000 / fps, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (global_i == dates.getImages().size()) {
                    global_i = 0;
                }
                if (dates.getFilter().equals("binaritza")) {
                    lbl_image.setIcon(new ImageIcon(f.binaritza_imatge(dates.getImages().get(global_i++).getBufImg(), optionsFrame.getSliderB())));
                } else if (dates.getFilter().equals("enfosqueix")) {
                    lbl_image.setIcon(new ImageIcon(f.enfosqueix_imatge(dates.getImages().get(global_i++).getBufImg(), optionsFrame.getSliderE())));
                } else {
                    lbl_image.setIcon(new ImageIcon(dates.getImages().get(global_i++).getBufImg()));
                }
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        txt_pathToZip = new javax.swing.JTextField();
        btn_openZip = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        lbl_image = new javax.swing.JLabel();
        jToolBar1 = new javax.swing.JToolBar();
        btn_ff = new javax.swing.JButton();
        tgl_play = new javax.swing.JToggleButton();
        btn_stop = new javax.swing.JButton();
        btn_rew = new javax.swing.JButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        btn_clear = new javax.swing.JButton();
        tgl_config = new javax.swing.JToggleButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jCheckBoxMenuItem1 = new javax.swing.JCheckBoxMenuItem();
        jMenu3 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        txt_pathToZip.setFont(new java.awt.Font("Lucida Grande", 0, 10)); // NOI18N
        txt_pathToZip.setText("Path to Zip");
        txt_pathToZip.setSelectionColor(new java.awt.Color(255, 255, 255));
        txt_pathToZip.setVerifyInputWhenFocusTarget(false);

        btn_openZip.setText("Open Zip");
        btn_openZip.setFocusable(false);
        btn_openZip.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_openZip.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_openZip.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_openZipActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(txt_pathToZip, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 255, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(btn_openZip, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 81, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(txt_pathToZip)
                    .add(btn_openZip, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 41, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Video:"));

        lbl_image.setBackground(new java.awt.Color(102, 0, 102));
        lbl_image.setForeground(new java.awt.Color(102, 153, 255));
        lbl_image.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_image.setText("Real time effects! Try it!");

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        btn_ff.setIcon(new javax.swing.ImageIcon(getClass().getResource("/view/res/left.png"))); // NOI18N
        btn_ff.setBorderPainted(false);
        btn_ff.setFocusable(false);
        btn_ff.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_ff.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_ff.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ffActionPerformed(evt);
            }
        });
        jToolBar1.add(btn_ff);

        tgl_play.setIcon(new javax.swing.ImageIcon(getClass().getResource("/view/res/play.png"))); // NOI18N
        tgl_play.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tgl_playMouseClicked(evt);
            }
        });
        tgl_play.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tgl_playActionPerformed(evt);
            }
        });
        jToolBar1.add(tgl_play);

        btn_stop.setIcon(new javax.swing.ImageIcon(getClass().getResource("/view/res/stop.png"))); // NOI18N
        btn_stop.setBorderPainted(false);
        btn_stop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_stopActionPerformed(evt);
            }
        });
        jToolBar1.add(btn_stop);

        btn_rew.setIcon(new javax.swing.ImageIcon(getClass().getResource("/view/res/right.png"))); // NOI18N
        btn_rew.setBorderPainted(false);
        btn_rew.setFocusable(false);
        btn_rew.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_rew.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_rew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_rewActionPerformed(evt);
            }
        });
        jToolBar1.add(btn_rew);
        jToolBar1.add(filler1);

        btn_clear.setText("Clear");
        btn_clear.setFocusable(false);
        btn_clear.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_clear.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_clear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_clearActionPerformed(evt);
            }
        });
        jToolBar1.add(btn_clear);

        tgl_config.setIcon(new javax.swing.ImageIcon(getClass().getResource("/view/res/config.png"))); // NOI18N
        tgl_config.setFocusable(false);
        tgl_config.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tgl_config.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tgl_config.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tgl_configActionPerformed(evt);
            }
        });
        jToolBar1.add(tgl_config);

        org.jdesktop.layout.GroupLayout jPanel3Layout = new org.jdesktop.layout.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel3Layout.createSequentialGroup()
                .add(jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel3Layout.createSequentialGroup()
                        .add(45, 45, 45)
                        .add(jToolBar1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .add(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .add(lbl_image, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 320, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel3Layout.createSequentialGroup()
                .add(14, 14, 14)
                .add(lbl_image, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 240, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jToolBar1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 37, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jMenu1.setText("File");

        jMenuItem2.setText("About");
        jMenu1.add(jMenuItem2);

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_ESCAPE, 0));
        jMenuItem1.setText("Exit");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Preferences");

        jCheckBoxMenuItem1.setText("Skin");
        jCheckBoxMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxMenuItem1ActionPerformed(evt);
            }
        });
        jMenu2.add(jCheckBoxMenuItem1);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Help");
        jMenu3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu3MouseClicked(evt);
            }
        });
        jMenu3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu3ActionPerformed(evt);
            }
        });
        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(layout.createSequentialGroup()
                        .add(jPanel3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(14, 14, 14)
                .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tgl_playActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tgl_playActionPerformed
    }//GEN-LAST:event_tgl_playActionPerformed

    private void tgl_playMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tgl_playMouseClicked

        if (tgl_play.isSelected()) {
//            t.setDelay(1000/optionsFrame.getFPS());
            if (loaded) {
                System.out.println("started");
                t.start();
                tgl_play.setIcon(new javax.swing.ImageIcon(getClass().getResource("/view/res/pause.png"))); // NOI18N


            } else {
                //custom title, error icon
                JOptionPane.showMessageDialog(this,
                        "You didn't load any image. Please, press Open Zip button",
                        "Play error",
                        JOptionPane.ERROR_MESSAGE);
                tgl_play.setSelected(false);

            }
        } else {
            tgl_play.setIcon(new javax.swing.ImageIcon(getClass().getResource("/view/res/play.png"))); // NOI18N
            t.stop();
        }
    }//GEN-LAST:event_tgl_playMouseClicked

    private void btn_stopActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_stopActionPerformed

        if (tgl_play.isSelected()) {
            tgl_play.setSelected(false);
            t.stop();
            global_i = 0;
            tgl_play.setIcon(new javax.swing.ImageIcon(getClass().getResource("/view/res/play.png")));
        }



    }//GEN-LAST:event_btn_stopActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        this.dispose();
        System.exit(0);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void btn_openZipActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_openZipActionPerformed
        String path;
        JFileChooser fc = new JFileChooser();
        fc.setFileFilter(new FileNameExtensionFilter("ZIP", "zip"));//filtrem nomes arxius zip
        fc.setCurrentDirectory(new java.io.File("../"));
        int returnVal = fc.showDialog(null, "Select zip");
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            path = fc.getSelectedFile().getParent() + "/" + fc.getSelectedFile().getName();
            dates.setImages(zipr.imageReader(path));
            dates.setNB_OF_IMAGES(dates.getImages().size());
            txt_pathToZip.setText(path);
            if (dates.getImages().size() > 0) { //si hi ha imatges, posem la primera
                loaded = true;
                lbl_image.setIcon(new ImageIcon(dates.getImages().get(global_i++).getBufImg()));
            }

        } else if (returnVal == JFileChooser.ERROR_OPTION) {
            JOptionPane.showMessageDialog(null, "Error selecting file", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "File selection canceled", "Cancel", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btn_openZipActionPerformed

    private void tgl_configActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tgl_configActionPerformed

        if (tgl_config.isSelected()) {

            optionsFrame.setVisible(true);

        } else {
            optionsFrame.setVisible(false);
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_tgl_configActionPerformed

    private void btn_clearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_clearActionPerformed
        if (loaded) {
            t.stop();
            dates.getImages().clear();
            lbl_image.setIcon(null);
            loaded = false;
            if (tgl_play.isSelected()) {
                tgl_play.setSelected(false);
                tgl_play.setIcon(new javax.swing.ImageIcon(getClass().getResource("/view/res/play.png")));
            }
        }
        optionsFrame.cleanStats();
    }//GEN-LAST:event_btn_clearActionPerformed

    private void btn_rewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_rewActionPerformed
        int actFTP = t.getDelay();
//        System.out.println("a " +actFTP);
        t.setDelay(actFTP - 5);
    }//GEN-LAST:event_btn_rewActionPerformed

    private void btn_ffActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ffActionPerformed
        int actFTP = t.getDelay();
        t.setDelay(actFTP + 5);
    }//GEN-LAST:event_btn_ffActionPerformed

    private void jCheckBoxMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxMenuItem1ActionPerformed
        int red = 222;
        int green = 243;
        int blue = 255;
        try {
            UIManager.put("nimbusBase", new Color(red, green, blue));
            UIManager.put("nimbusBlueGrey", new Color(red, green, blue));
            UIManager.put("control", new Color(red, green, blue));
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    this.setVisible(false);
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    this.setVisible(true);
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }



    }//GEN-LAST:event_jCheckBoxMenuItem1ActionPerformed

    private void jMenu3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu3ActionPerformed
    }//GEN-LAST:event_jMenu3ActionPerformed

    private void jMenu3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu3MouseClicked
        JOptionPane.showMessageDialog(null, "- Arrows let you play with FPS of player\n"
                + "- Use clear to start from scratch\n"
                + "- Open Options to see more features\n"
                + "- Use Esc key on your keyboard to exit quickly", "Some useful tricks", JOptionPane.PLAIN_MESSAGE);

    }//GEN-LAST:event_jMenu3MouseClicked
    /**
     * Metode per actualitzar el delay del timer
     *
     * @param FPS
     */
    void setFps(int FPS) {
        t.setDelay(1000 / FPS);
    }

    void setLoaded(boolean b) {
        loaded = b;
    }

    public OptionsWindow getOptionsFrame() {
        return optionsFrame;
    }

    public void setOptionsFrame(OptionsWindow optionsFrame) {
        this.optionsFrame = optionsFrame;
    }

    public Data getD() {
        return dates;
    }

    public void setD(Data d) {
        this.dates = d;
    }

    public int getGlobal_i() {
        return global_i;
    }

    public void setGlobal_i(int global_i) {
        this.global_i = global_i;
    }

    public ZipGzip getZipr() {
        return zipr;
    }

    public void setZipr(ZipGzip zipr) {
        this.zipr = zipr;
    }

    public Filter getF() {
        return f;
    }

    public void setF(Filter f) {
        this.f = f;
    }

    public Timer getT() {
        return t;
    }

    public void setT(Timer t) {
        this.t = t;
    }

    public JButton getBtn_clear() {
        return btn_clear;
    }

    public void setBtn_clear(JButton btn_clear) {
        this.btn_clear = btn_clear;
    }

    public JButton getBtn_openZip() {
        return btn_openZip;
    }

    public void setBtn_openZip(JButton btn_openZip) {
        this.btn_openZip = btn_openZip;
    }

    public JButton getBtn_stop() {
        return btn_stop;
    }

    public void setBtn_stop(JButton btn_stop) {
        this.btn_stop = btn_stop;
    }

    public ButtonGroup getButtonGroup1() {
        return buttonGroup1;
    }

    public void setButtonGroup1(ButtonGroup buttonGroup1) {
        this.buttonGroup1 = buttonGroup1;
    }

    public Filler getFiller1() {
        return filler1;
    }

    public void setFiller1(Filler filler1) {
        this.filler1 = filler1;
    }

    public JMenu getjMenu1() {
        return jMenu1;
    }

    public void setjMenu1(JMenu jMenu1) {
        this.jMenu1 = jMenu1;
    }

    public JMenuBar getjMenuBar1() {
        return jMenuBar1;
    }

    public void setjMenuBar1(JMenuBar jMenuBar1) {
        this.jMenuBar1 = jMenuBar1;
    }

    public JMenuItem getjMenuItem1() {
        return jMenuItem1;
    }

    public void setjMenuItem1(JMenuItem jMenuItem1) {
        this.jMenuItem1 = jMenuItem1;
    }

    public JPanel getjPanel3() {
        return jPanel3;
    }

    public void setjPanel3(JPanel jPanel3) {
        this.jPanel3 = jPanel3;
    }

    public JToolBar getjToolBar1() {
        return jToolBar1;
    }

    public void setjToolBar1(JToolBar jToolBar1) {
        this.jToolBar1 = jToolBar1;
    }

    public JLabel getLbl_image() {
        return lbl_image;
    }

    public void setLbl_image(JLabel lbl_image) {
        this.lbl_image = lbl_image;
    }

    public JToggleButton getTgl_config() {
        return tgl_config;
    }

    public void setTgl_config(JToggleButton tgl_config) {
        this.tgl_config = tgl_config;
    }

    public JToggleButton getTgl_play() {
        return tgl_play;
    }

    public void setTgl_play(JToggleButton tgl_play) {
        this.tgl_play = tgl_play;
    }

    public JTextField getTxt_pathToZip() {
        return txt_pathToZip;
    }

    public void setTxt_pathToZip(JTextField txt_pathToZip) {
        this.txt_pathToZip = txt_pathToZip;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainWindow().setVisible(true);

            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_clear;
    private javax.swing.JButton btn_ff;
    private javax.swing.JButton btn_openZip;
    private javax.swing.JButton btn_rew;
    private javax.swing.JButton btn_stop;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JLabel lbl_image;
    private javax.swing.JToggleButton tgl_config;
    private javax.swing.JToggleButton tgl_play;
    private javax.swing.JTextField txt_pathToZip;
    // End of variables declaration//GEN-END:variables
}
