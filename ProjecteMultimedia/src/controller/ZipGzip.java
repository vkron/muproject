/**
 * Copyrights Project name: GerVit Codec
 *
 * @author Vitaliy Konovalov
 * @author Gerard Rodriguez University of Barcelona, 2012-2013
 */
package controller;

import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.imageio.ImageIO;
import model.Data;
import view.MainWindow;

/**
 * ZipGzip class to read zip with images
 */
public class ZipGzip {

    Data dates;

    public ZipGzip(Data d) {
        this.dates = d;
    }

    /**
     * Donat un short, el passa a un array de bytes
     *
     * @param x
     * @return
     */
    private byte[] shortToBytes(short x) {

        ByteBuffer b = ByteBuffer.allocate(2);
        //b.order(ByteOrder.BIG_ENDIAN); // optional, the initial order of a byte buffer is always BIG_ENDIAN.
        b.putShort(x);

        return b.array();

    }

    /**
     * Donat un array de bytes el passa a short
     *
     * @param val
     * @return
     */
    private short bytesToShort(byte[] val) {
        ByteBuffer bb = ByteBuffer.allocate(2);
        bb.put(val);
        bb.rewind();
        return bb.getShort();
    }

    /**
     * Opens zip file and read its entries as images loading them to
     * ArrayList<pTSM>
     *
     * @return ArrayList<pTSM> imatges
     */
    public ArrayList<pTSM> imageReader(String path) {
        ArrayList<pTSM> imatges = new ArrayList<pTSM>();
        FileInputStream fis = null;
        int bufferSize = 9000;
        int count;
        ZipInputStream zis;
        ZipEntry entry;
        pTSM p = new pTSM();
        FileOutputStream fos = null;
        FileInputStream fin;
        BufferedOutputStream dest;
        byte[] data = new byte[bufferSize];
        try {
//            System.out.println("Intentando crear en " + path);
            fis = new FileInputStream(path);
            zis = new ZipInputStream(new BufferedInputStream(fis));
            // un cop oberts els streams d'entrada podem llegir el zip
            while ((entry = zis.getNextEntry()) != null) {
                if (!entry.isDirectory()) {
                    //obrim streams de sortida per poder escriure imatges
                    fos = new FileOutputStream("./temp.png");
                    dest = new BufferedOutputStream(fos, bufferSize);
                    //mentre queden bytes, anem llegint la imatge i escribintla a
                    //arxiu temporal
                    while ((count = zis.read(data, 0, bufferSize)) != -1) {
                        dest.write(data, 0, count);
                        dest.flush();
                    }
                    dest.close();
                    fos.close();
                    //un cop llegida i guardada en temporal, la escribim en format png
                    fin = new FileInputStream("./temp.png");
                    BufferedImage bi = ImageIO.read(fin);
                    bi.flush();
                    fin.close();
                    p = new pTSM(bi, entry.getName());
                    imatges.add(p);
                }
                //si era directori , saltem a seguent entrada.
                entry = zis.getNextEntry();
            }
            zis.close();//tanquem streams
            fis.close();
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
        return imatges;
    }

    /**
     * Donada una estructura que conté una seqüencia dates'imatges i un path.
     * Guarda a disc aquesta seqüencia comprimint en JPEG + GZIP
     *
     * @param images
     * @param path
     */
    public void saveGzip(ArrayList<pTSM> images, String path) {
        short numImatges = 0;
        BufferedImage im = null;
        if (images != null) {
            numImatges = (short) images.size();
        }
        try {
            FileOutputStream fos = new FileOutputStream(path);
            GZIPOutputStream gzip = new GZIPOutputStream(fos);
            gzip.write(shortToBytes(numImatges));//lo primer que escribim es el numero de imatges
            gzip.write(shortToBytes((short) 0));//lo segon es el nombre de vectors 
            System.out.println("Writing numImatges=" + numImatges + " numVectors=" + 0);


            for (int i = 0; i < dates.getNB_OF_IMAGES(); i++) {

                if (images != null) {
                    im = images.get(i).getBufImg();
                }
                ByteArrayOutputStream byteOstream = new ByteArrayOutputStream();
                ImageIO.write(im, "jpeg", byteOstream);
//                System.out.println("Writing..." + byteOstream.toByteArray().length);
                byteOstream.flush();
                byteOstream.close();
                gzip.write(shortToBytes((short) byteOstream.size()));//mida imatge en bytes
//                System.out.println("mida:" + byteOstream.size());
                gzip.write(byteOstream.toByteArray()); // imatge en si
            }
            gzip.flush();
            gzip.finish();
            gzip.close();
            fos.close();
        } catch (IOException ex) {
            System.err.println("IOException:" + ex.getMessage());
        }
        System.out.println("File successfully saved to disk");

    }

    /**
     * Donat el video comprimit i un path, guarda el video en format nostre .grv
     *
     * @param vidCompr
     * @param path
     * @return number of KB saved to disk
     */
    public void saveGzipCompressed(VideoComprimit vidCompr, String path, int tesSize) {
        short numImatges = 0;
        short numVectors = 0;


        BufferedImage im = null;
        if (vidCompr != null) {
            numImatges = (short) vidCompr.size();//guardem mides
            numVectors = (short) vidCompr.getVector(0).size();

        }
        try {
            GZIPOutputStream gzip_out = new GZIPOutputStream(new FileOutputStream(path));

            System.out.println("Writing numImatges=" + numImatges + " numVectors=" + numVectors);
            gzip_out.write(shortToBytes(numImatges));//lo primer que escribim es el numero de imatges
            gzip_out.write(shortToBytes(numVectors));//lo segon es el nombre de vectors 
            gzip_out.write(shortToBytes((short) tesSize));//lo segon es el nombre de vectors 

            for (int i = 0; i < dates.getNB_OF_IMAGES(); i++) {
                if (vidCompr != null) {
                    if (i < vidCompr.size()) {
                        im = vidCompr.getImatge(i).toBufferedImage();
                    }
                }
                ByteArrayOutputStream byteOstream = new ByteArrayOutputStream();
                ImageIO.write(im, "jpg", byteOstream);
                byteOstream.flush();
                byteOstream.close();

                System.out.println("Writing..." + byteOstream.toByteArray().length);

                gzip_out.write(shortToBytes((short) byteOstream.size()));//mida imatge en bytes
                gzip_out.write(byteOstream.toByteArray());//la imatge
            }

            //un cop s'han escrit les imatges, escrivim vectors
            System.out.println("Vectors now..\n");
            for (int i = 0; i < numImatges - 1; i++) {// posem size()-1 perque la primera imatge no te vectors
                ArrayList<Vector> vecs = vidCompr.getVector(i);
//                System.out.println("w vec i = " + i);

                for (Vector v : vecs) {
                    if (v != null) {
                        gzip_out.write(shortToBytes((short) v.getX()));
                        gzip_out.write(shortToBytes((short) v.getY()));
//                        System.out.println(" x:" + v.getX() + " y:" + v.getY());
                    } else {
//                        System.out.println("NULL");
//                        System.out.print("x:writing 0xFFFFFFFF ");
//                        System.out.println("y:writing 0xFFFFFFFF");
                        gzip_out.write(shortToBytes((short) 32767));
                        gzip_out.write(shortToBytes((short) 32767));
                    }
//                    numKB += tmpBAOS.toByteArray().length;
//                    System.out.println("Writing: vec of size: " + tmpBAOS.toByteArray().length);  

                }
            }
            gzip_out.flush();
            gzip_out.finish();
            gzip_out.close();
        } catch (IOException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("\nFile successfully saved to disk");
    }

    /**
     * Donat un path, llegeix l'arxiu comprimit en gzip
     *
     * @param path
     */
    public void readGzip(String path) {
        int numVectors, numImatges;
        ArrayList<pTSM> ims = dates.getImages();
        if (ims != null) {
            if (!ims.isEmpty()) {
                ims.clear();
                dates.setImages(ims);
            }
        } else {
            dates.setImages(new ArrayList<pTSM>());
        }

        try {
            GZIPInputStream zis = new GZIPInputStream(new FileInputStream(path), 5000000);
            byte[] bytes = new byte[2];
            zis.read(bytes);
            numImatges = bytesToShort(bytes);
            zis.read(bytes);
            numVectors = bytesToShort(bytes);
            System.out.println("Read numImatges=" + numImatges + " numVectors=" + numVectors);
            while (dates.getImages().size() < numImatges) {
                bytes = new byte[2];
                zis.read(bytes);
                int midaImatge = bytesToShort(bytes);
//                System.out.println("midaIma:" + midaImatge);
                bytes = new byte[midaImatge];//llegim un array de bytes de mida com la imatge
                zis.read(bytes, 0, bytes.length);
                BufferedImage bi = ImageIO.read(new ByteArrayInputStream(bytes));
                pTSM im = new pTSM(bi);
                dates.add(im);

            }

            zis.close();

        } catch (IOException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);

        }

    }

    /**
     * Donat el path del video comprimit, el llegeix de disc
     *
     * @param path
     */
    public void readGzipCompressed(String path) {
        int tesSize, numVectors, numImatges;
        VideoComprimit tmvidComp = new VideoComprimit();
        ArrayList<pTSM> ims = dates.getImages();
        if (ims != null) {
            if (!ims.isEmpty()) {
                ims.clear();
                dates.setImages(ims);
            }
        } else {
            dates.setImages(new ArrayList<pTSM>());
        }
        ArrayList<Vector> vecs = null;
        ArrayList<ArrayList<Vector>> vectors;


        try {
            GZIPInputStream zis = new GZIPInputStream(new FileInputStream(path), 5000000);

            byte[] bytes = new byte[2];
            zis.read(bytes);
            numImatges = bytesToShort(bytes);
            zis.read(bytes);
            numVectors = bytesToShort(bytes);
            zis.read(bytes);
            tesSize = bytesToShort(bytes);
            System.out.println("Read numImatges=" + numImatges + " numVectors=" + numVectors);

            while (dates.getImages().size() < numImatges) {
                bytes = new byte[2];
                zis.read(bytes);
                int midaImatge = bytesToShort(bytes);
                bytes = new byte[midaImatge];
                zis.read(bytes, 0, bytes.length);
                BufferedImage bi = ImageIO.read(new ByteArrayInputStream(bytes));
                tmvidComp.afegeix(new Teseles(bi, tesSize));//afegim al video comprimit la imatge teselada
                pTSM im = new pTSM(bi);
                dates.add(im);
            }

            //ara es llegeixen els vectors
            vectors = new ArrayList<ArrayList<Vector>>();

            Vector x = null;
            for (int j = 0; j < numImatges - 1; j++) {
                vecs = new ArrayList<Vector>();
//                System.out.println("reading vecs at i=" + j);
                for (int i = 0; i < numVectors; i++) {

                    bytes = new byte[2];
                    zis.read(bytes);
                    int xx = bytesToShort(bytes);
                    bytes = new byte[2];
                    zis.read(bytes);
                    int yy = bytesToShort(bytes);
//                    System.out.println("read: x:"+xx+" y:"+yy);

                    if (xx != 32767 && yy != 32767) { //si no eren nulls
                        vecs.add(new Vector(xx, yy));
//                        System.out.println("  x:" + xx + " y:" + yy);
                    } else {
//                        System.out.println("NULL");
//                        System.out.print("else: x:FFFF ");
//                        System.out.println("y:FFFF");
                        vecs.add(null); //si era null
                    }


                }
//                System.out.println("vecs.size()"+vecs.size()); 
                tmvidComp.afegeix(vecs);
            }
            Codec c = new Codec();
            Video v = c.descompressioDCThard(tmvidComp);

            dates.setImages(v.toArray());
            zis.close();
        } catch (IOException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);

        }

    }
}
