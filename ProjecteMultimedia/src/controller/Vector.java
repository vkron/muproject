/**
 * Copyrights
 * Project name: GerVit Codec
 * @author Vitaliy Konovalov
 * @author Gerard Rodriguez
 * University of Barcelona, 2012-2013
 */
package controller;

import java.io.Serializable;

/**
 * Vector
 */
public class Vector implements Serializable{
    int x,y;

    public Vector(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
     
    
}
