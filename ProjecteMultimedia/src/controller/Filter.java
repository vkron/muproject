/**
 * Copyrights
 * Project name: GerVit Codec
 * @author Vitaliy Konovalov
 * @author Gerard Rodriguez
 * University of Barcelona, 2012-2013
 */
package controller;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;

/**
 * Filter: serveix per aplicar un cert filtre jugant amb threshold a la imatge.
 */
public class Filter {
    /**
     * Donada la imatge i un threshold, s'aplica una binarització
     * @param in imatge on aplicar 
     * @param treshold 
     * @return 
     */
    public BufferedImage binaritza_imatge(BufferedImage in, int treshold) {
        int height = in.getHeight();
        int width = in.getWidth();

        BufferedImage tmp = deepCopy(in);
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int green = new Color(tmp.getRGB(i, j)).getGreen();
                if (green < treshold) {
                    tmp.setRGB(i, j, (new Color(0, 0, 0)).getRGB());
                } else {
                    tmp.setRGB(i, j, (new Color(255, 255, 255)).getRGB());
                }
            }
        }

        return tmp;
    }
    /**
     * Donat un threshold i la imatge, es fa una pujada o baixada d'intensitat
     * @param in
     * @param treshold
     * @return 
     */
    public BufferedImage enfosqueix_imatge(BufferedImage in, int treshold) {
        int height = in.getHeight();
        int width = in.getWidth();

        BufferedImage tmp = deepCopy(in);
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int green = new Color(tmp.getRGB(i, j)).getGreen() * treshold / 10;
                int blue = new Color(tmp.getRGB(i, j)).getBlue() * treshold / 10;
                int red = new Color(tmp.getRGB(i, j)).getRed() * treshold / 10;

                if (green > 255) {
                    green = 255;
                }
                if (red > 255) {
                    red = 255;
                }
                if (blue > 255) {
                    blue = 255;
                }

                tmp.setRGB(i, j, (new Color(red, green, blue)).getRGB());
            }
        }
        return tmp;
    }
    static BufferedImage deepCopy(BufferedImage bi) {
        ColorModel cm = bi.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = bi.copyData(null);
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }
}
