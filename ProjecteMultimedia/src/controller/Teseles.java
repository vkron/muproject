/**
 * Copyrights
 * Project name: GerVit Codec
 * @author Vitaliy Konovalov
 * @author Gerard Rodriguez
 * University of Barcelona, 2012-2013
 */
package controller;

import java.awt.image.BufferedImage;
import java.util.ArrayList;


/**
 * Teseles
 * La classe tesseles representa una imatge partida en tesseles
 * 
 */
class Teseles {
    private ArrayList<ArrayList<Tesela>> tesseles;
    private int mida_tesseles;
    
    /**
     * Constructor de tesseles a partir d'una BufferedImage
     * 
     * @param a BufferedImage a convertir
     * @param mida Mida de les tesseles
     */
    public Teseles(BufferedImage a, int mida){
        tesseles = new ArrayList<ArrayList<Tesela>>();
        mida_tesseles = mida;        
        
        int width = a.getWidth()/mida;
        int height = a.getHeight()/mida;
        
        for (int i = 0; i < height; ++i) {
            ArrayList<Tesela> tmp = new ArrayList<Tesela>();
            for (int j = 0; j < width; ++j) {
                tmp.add(new Tesela(a.getSubimage(j*mida, i*mida, mida, mida)));                
            }
            tesseles.add(tmp);
        }   
    }
    
    /**
     * Aquest metode converteix les tesseles a una BufferedImage
     * @return Retorna les tesseles passades a BufferedImage
     */    
    public BufferedImage toBufferedImage() {
        int mida = this.mida_tesseles;
        int width = this.getWidth();
        int height = this.getHeight();
//        System.out.println("Reconstruir imatge: "+width+"*"+height+"*"+mida);
        BufferedImage resultat = new BufferedImage(width*mida,height*mida,1);
        
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                BufferedImage imatge = this.get(i, j).getImatge();
                for (int x = 0; x < imatge.getWidth(); ++x) {
                    for (int y = 0; y < imatge.getHeight(); ++y) {
                        int rgb = imatge.getRGB(x, y);
                        resultat.setRGB(mida*j+x,mida*i+y, rgb);
                    }
                }                
            }
        }
        
        return resultat;
    }
    
    public Teseles(Teseles a) { 
       
        tesseles = new ArrayList<ArrayList<Tesela>>(a.getHeight());
        for (int i = 0; i < a.getHeight(); ++i) {
            ArrayList<Tesela> tmp = new ArrayList<Tesela>(a.getWidth());
            for (int e = 0; e < a.getWidth(); ++e) {
                tmp.add(new Tesela(a.get(i, e).getImatge()));
            }            
            tesseles.add(tmp);
        }     
        
        mida_tesseles = a.mida_tesseles;
    }

    
    public Tesela get(int i, int j) {
        ArrayList<Tesela> pts = tesseles.get(i);
        return pts.get(j);
    }    
    
    public int getWidth(){
        return tesseles.get(0).size();
    }

    public int getHeight() {
        return tesseles.size();
    }
    
    public void set(int i, int j, Tesela tin) {        
        ArrayList<Tesela> pts = tesseles.get(i);        
        pts.set(j, tin);
        tesseles.set(i, pts);        
    }   

    /**
     * Aquest metode posa a negre les tesseles marcades pel vector si op == 1
     * i les mou i deixa les altres a negre si op == 2
     * 
     * @param vectors
     * @return 
     */
    public Teseles mou(ArrayList<Vector> vectors, int op) {        
        int width = this.getWidth();
        int height = this.getHeight();
        //Teseles res = new Teseles(width, height, this.mida_tesseles);
        Teseles res = new Teseles(this);        
        Tesela negre = this.get(0,0).negre(); //retorna una tessela negre
        
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (vectors.get((i * width) + j) != null) {
                    
                    int vecX = vectors.get((i * width) + j).getX();
                    int vecY = vectors.get((i * width) + j).getY();

                    if (op == 1) {
                        res.set(i, j, negre);
                    } else {
//                        System.out.println("Vec: "+vecX+" "+vecY);
                        res.set(i, j, this.get(i + vecX, j + vecY));
                    }

                } else {
                    if (op == 1) {
                        res.set(i, j, this.get(i, j));
                    } else {
                        res.set(i, j, negre);
                    }
                }

            }
        }
        
        return res;        
    }
    
    /**
     * Estimacio del moviment
     * Aquest metode retorna l'array de vectors que porten de cada tessela
     * de this.tesseles a la tessela d'objectiu que mes s'hi assembla
     * 
     * @param objectiu Imatge a comparar
     * @return Array de vectors que porten des de l'imatge actual a objectiu
     */
    
    public ArrayList<Vector> estima(Teseles objectiu, double precisio, double espiral) {
        ArrayList<Vector> res = new ArrayList<Vector>();
        
        //Per a cada tessela es busca en el cuadre desti si hi ha alguna 
        //coincidencia
        //
        //Es guarda la coincidencia com a vector
      
        for (int i = 0; i < objectiu.getHeight(); i++) {            
            for (int j = 0; j < objectiu.getWidth(); j++) {
                res.add(this.busca(objectiu.get(i,j),precisio,i,j,espiral));
            }
        }              
        return res;
    }
    
    /**
     * Aquest metode retorna la posicio de la Tesela dins de this.tesseles
     * que mes s'assembla a la tesela objectiu
     * 
     * Utilitza una busca en espiral per tal de trobar una tessela semblant
     * mes rapidament
     * 
     * @param Tesela a buscar
     * @return Posicio de la tessela mes semblant a objectiu
     */
    private Vector busca(Tesela objectiu, double precisio,int ii, int jj, double espiral_iter) {
 
        int i = ii;
        int j = jj;
        int height = this.getHeight();
        int width = this.getWidth();
        int distancia_amunt = Math.max(jj, height - jj);
        int distancia_abaix = Math.max(ii, width - ii);
        int num_iteracions = 2*Math.max(distancia_amunt, distancia_abaix);        
        int iteracions = 1;

        while (iteracions <= (espiral_iter/100)*num_iteracions) {
            int signe = 1;
            if ((iteracions % 2) == 0) {
                signe = -1;
            }

            for (int e = 0; e < iteracions; ++e) {
                if (j < width && j >= 0 && i < height && i >= 0) {
                    if (this.get(i, j).compara(objectiu, precisio)) {                        
                        return new Vector(i - ii, j - jj);
                    }
                }
                j += signe;
            }
            for (int e = 0; e < iteracions; ++e) {
                if (j < width && j >= 0 && i < height && i >= 0) {
                    if (this.get(i, j).compara(objectiu, precisio)) {                        
                        return new Vector(i - ii, j - jj);
                    }
                }
                i -= signe;
            }

            iteracions++;
        }
        return null;
    }
    
    /**
     * Compensacio del moviment
     * Aquest metode retorna la imatge residual donada per la resta entre
     * l'imatge actual i l'imatge anterior desplaçada pels vectors
     * 
     * @param vectors
     * @param anterior
     * @return Imatge residual Actual - anterior moguda vectors
     */

    public Teseles compensa(Teseles anterior, ArrayList<Vector> vectors) {        
        Teseles mogut = this.mou(vectors,1);        
        //return this.resta(mogut);
        return mogut;
    }
    
    /**
     * Aquest metode retorna la imatge reconstruida a partir de l'imatge residual
     * i la imatge donada desplaçada amb els vectors donats.
     * 
     * @param im imatge desplaçada amb vecs a sumar amb la residual
     * @param vecs vectors de desplaçament
     * @return imatge reconstruida
     */
    
    public Teseles reconstrueix(Teseles im, ArrayList<Vector> vectors) {                
        Teseles mogut = im.mou(vectors,2);                
        return this.suma(mogut);
    }    

    public Teseles suma(Teseles a) {
        Teseles res = new Teseles(a);       
        
        for (int i = 0; i < a.getHeight(); i++) {
            for (int j = 0; j < a.getWidth(); j++) {                
                res.set(i,j, this.get(i,j).suma(a.get(i, j)));
            }
        }
        return res;
    }    
}
