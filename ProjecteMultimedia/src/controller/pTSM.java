package controller;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

/**
 * Classe que contindra les dades refernts a les imatges 
 * @author JDPrades
 * @author Jordi Montserrat
 * @author David Barber
 */
public class pTSM{
	/** Buffer de la imatge capturada */
	private BufferedImage bufImage;
	/** Nom de la imatge, img001, img0012 ... */
	private String nomImatge;
	/** Amplada de la Imatge */
	private int amplada;
	/** Altura de la Imatge */
	private int altura;

	/**
	 * Constructor buit de la classe pTSM
	 */
	public pTSM(){
	}
	
	/**
	 * Constructor de la classe pTSM
	 * @param n, Nom de la imatge
	 */
	public pTSM(String n){
		nomImatge = n;
	}
	/**
	 * Constructor de la classe pTSM
	 * @param bi, Imatge.
	 
	 */
	public pTSM(BufferedImage bi) {
		bufImage = bi;
		 
		altura = bi.getHeight();
		amplada = bi.getWidth();
	}
	/**
	 * Constructor de la classe pTSM
	 * @param bi, Imatge.
	 * @param ni, Nom de a imatge.
	 */
	public pTSM(BufferedImage bi, String ni) {
		bufImage = bi;
		nomImatge = ni;
		altura = bi.getHeight();
		amplada = bi.getWidth();
	}
	
	/**
	 * Metode per assignar una imatge
	 * @param bi Imatge
	 */
	public void setBufImg(BufferedImage bi){
		bufImage = bi;
	}
	
	/**
	 * Metode que retornara la imatge
	 * @return la imatge
	 */
	public BufferedImage getBufImg() {
		return bufImage;
	}
	
	/**
	 * Metode que retornara el nom de la imatge
	 * @return el nom de la imatge
	 */
	public String getNomImatge() {
		return nomImatge;
	}

	/**
	 * Metode que utilitzarem per pintar la imatge al canvas
	 * @param g, Grafics
	 */
	public void draw(Graphics g) {
		g.drawImage(bufImage, 0, 0, amplada, altura, null);
	}
}