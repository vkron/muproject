/**
 * Copyrights Project name: GerVit Codec
 *
 * @author Vitaliy Konovalov
 * @author Gerard Rodriguez University of Barcelona, 2012-2013
 */
package controller;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Video: classe que conte la sequencia de fotogrames. Permet afegir un nou
 * fotograma o consultar un
 */
public class Video {

    ArrayList<BufferedImage> fotogrames;

    /**
     * Constructor que inicialitza l'estructura contenidora de fotogrames
     */
    public Video() {
        fotogrames = new ArrayList<BufferedImage>();
    }

    /**
     * Constructor amb parametre la secuencia de fotogrames
     *
     * @param video
     */
    public Video(ArrayList<pTSM> video) {
        fotogrames = new ArrayList<BufferedImage>();
        for (int i = 0; i < video.size(); i++) {
            fotogrames.add(video.get(i).getBufImg());
        }

    }

    /**
     * Afegeix un nou fotograma a la secuencia de video
     *
     * @param a
     */
    public void afegeixFotograma(BufferedImage a) {
        fotogrames.add(a);
    }

    /**
     * Retorna un fotograma de la secuencia de video
     *
     * @param i
     * @return fotograma BufferedImage en la posicio i
     */
    public BufferedImage get(int i) {
        return fotogrames.get(i);
    }

    /**
     * Permet consultar la mida de la secuencia. Quantes fotogrames te.
     * @return
     */
    public int size() {
        return fotogrames.size();
    }
    /**
     * Retorna un ArrayList<pTSM> amb tots els fotogrames
     * @return ArrayList<pTSM>
     */
    public ArrayList<pTSM> toArray() {
        ArrayList<pTSM> res = new ArrayList<pTSM>();
        for (int i = 0; i < this.size(); i++) {
            res.add(new pTSM(fotogrames.get(i)));
        }

        return res;
    }
}
