/**
 * Copyrights
 * Project name: GerVit Codec
 * @author Vitaliy Konovalov
 * @author Gerard Rodriguez
 * University of Barcelona, 2012-2013
 */
package controller;

import java.awt.Color;
import java.awt.image.BufferedImage;

/**
 * Tesela
 */
class Tesela {
    BufferedImage imatge;    

    Tesela() {
        imatge = null;
    }

    public BufferedImage getImatge() {
        return imatge;
    }
   
    Tesela(BufferedImage image) {
        imatge = image;
    }    
    
    private BufferedImage suma(BufferedImage b) {
        BufferedImage a = this.imatge;
        int width = a.getWidth();
        int height = a.getHeight();
        BufferedImage resultat = new BufferedImage(width,height,a.getType());
        
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int color = a.getRGB(i,j)+b.getRGB(i,j);            
                resultat.setRGB(i, j, color);                
            }
        }        
        return resultat; 
    }        
    
    public Tesela suma(Tesela b) {       
        return new Tesela(this.suma(b.imatge));        
    }
    
    public Boolean compara(Tesela b, double exactitud) {
        BufferedImage imagen = this.getImatge();
        BufferedImage comparada = b.getImatge();
        double mida = imagen.getWidth()*imagen.getHeight();
        double red1 = 0, green1 = 0, blue1 = 0, red2 = 0, green2 = 0, blue2 = 0;


        for (int i = 0; i < imagen.getWidth(); i++) {
            for (int j = 0; j < imagen.getHeight(); j++) {
                red1 += new Color(imagen.getRGB(i, j)).getRed();
                green1 += new Color(imagen.getRGB(i, j)).getGreen();
                blue1 += new Color(imagen.getRGB(i, j)).getBlue();

                red2 += new Color(comparada.getRGB(i, j)).getRed();
                green2 += new Color(comparada.getRGB(i, j)).getGreen();
                blue2 += new Color(comparada.getRGB(i, j)).getBlue();
            }
        }

        return ((Math.abs(red1 - red2)/mida < exactitud)
                && (Math.abs(green1 - green2)/mida < exactitud)
                && (Math.abs(blue1 - blue2)/mida < exactitud));

    }
    
    Tesela negre() {
        BufferedImage tmp = this.getImatge();
        for (int i = 0; i < tmp.getWidth(); i++) {
            for (int j = 0; j < tmp.getHeight(); j++) {
                tmp.setRGB(i, j, 0);
            }
        }
        
        return new Tesela(tmp);
    }
}
