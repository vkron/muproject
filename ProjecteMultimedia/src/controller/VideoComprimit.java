/**
 * Copyrights Project name: GerVit Codec
 *
 * @author Vitaliy Konovalov
 * @author Gerard Rodriguez University of Barcelona, 2012-2013
 */
package controller;

import java.util.ArrayList;

/**
 * VideoComprimit: classe que conte les tesseles i els vectors. És a dir, la
 * sequencia de video comprimida mitjançant un algorisme
 */
public class VideoComprimit {

    ArrayList<Teseles> imatges;
    ArrayList<ArrayList<Vector>> vectors;

    /**
     * Constructor buit que inicialitza les estructures
     */
    public VideoComprimit() {
        imatges = new ArrayList<Teseles>();
        vectors = new ArrayList<ArrayList<Vector>>();
    }

    /**
     * Permet consultar les Teseles d'una imatge
     * @param i
     * @return Teseles d'una imatge en posició i
     */
    public Teseles getImatge(int i) {
        return imatges.get(i);
    }
    /**
     * Consultar vectors en posició i
     * @param i
     * @return ArrayList<Vector> en posició i
     */
    public ArrayList<Vector> getVector(int i) {
        return vectors.get(i);
    }
    /**
     * Afegeix nova imatge com Teseles en l'estructura 
     * @param a Teseles
     */
    public void afegeix(Teseles a) {
        imatges.add(a);
    }
    /**
     * Afegeix els vectors en l'estructura vector
     * @param a ArrayList<Vector>
     */
    public void afegeix(ArrayList<Vector> a) {
        vectors.add(a);
    }
    /**
     * Consultar la mida del video comprimit
     * @return int que diu la quantitat d'imatges en video
     */
    public int size() {
        return imatges.size();
    }
}
