/**
 * Copyrights
 * Project name: GerVit Codec
 * @author Vitaliy Konovalov
 * @author Gerard Rodriguez
 * University of Barcelona, 2012-2013
 */
package controller;

import java.util.ArrayList;

/**
 * La classe codec conté els mètodes compressioDCThard i  descompressioDCThard
 * que comprimeixen i descomprimeixen videos respectivament
 */
public class Codec {   
   /**
    * Aquest metode comprimeix el video donat amb la mida de tesseles especificada
    * Retorna un VideoComprimit que conté les imatges residuals i els vectors. 
    * 
    * @param images Video a comprimir
    * @param mida_tesseles Mida de les tesseles
    * @param precisio Treshold al comparar les imatges
    * @param espiral  % d'imatge que es recorrera amb la busca espiral
    * @return Video comprimit
    */
    public VideoComprimit compressioDCThard(Video video, int mida_tesseles, double precisio, double espiral) {
        
        System.out.println("codec:Comprimint...");
        VideoComprimit result = new VideoComprimit();        
        
        //Guardem la primera imatge del video
        result.afegeix(new Teseles(video.get(0),mida_tesseles));
        
        //Per a cada imatge restant        
        for (int i = 1; i < video.size(); i++) {
            
            Teseles actual = new Teseles(video.get(i), mida_tesseles);      
            Teseles anterior = new Teseles(video.get(i-1), mida_tesseles);        
            
            //estimem els vectors que van des de anterior fins a actual.
            ArrayList<Vector> vectors = actual.estima(anterior, precisio, espiral);
            
            //Creem la imatge residual a partir dels vectors
            Teseles residual = actual.compensa(anterior, vectors);
            
            //Afegim al resultat els vectors i l'imatge residual
            result.afegeix(vectors);
            result.afegeix(residual);      
            
            System.out.println(i);
        }        
        System.out.println("codec:Comprimit!");
        return result;
    }
    
    /**
     * Aquest metode descomprimeix el video comprimit donat
     *
     * @param d Video comprimit     
     * @return Video descomprimit
     */
    public Video descompressioDCThard(VideoComprimit d) {
        System.out.println("codec:Desomprimint...");

        //Retornarem un Video comprimit
        ArrayList<Teseles> tmp = new ArrayList<Teseles>();
        Video resultat = new Video();
        
        //Recuperem la primera imatge    
        tmp.add(d.getImatge(0));
        resultat.afegeixFotograma(tmp.get(0).toBufferedImage());

        //Per a la resta d'imatges residuals i vectors
        for (int i = 1; i < d.size(); i++) {
            
            Teseles actual = d.getImatge(i); 
            Teseles anterior = tmp.get(i - 1);
            ArrayList<Vector> vectors = d.getVector(i - 1);
            
            //Afegim al resultat la suma de la imatge anterior moguda amb els vectors
            //i la imatge residual actual
            tmp.add(actual.reconstrueix(tmp.get(i-1),vectors));
            resultat.afegeixFotograma(tmp.get(i).toBufferedImage());            
        }

        System.out.println("codec:Desomprimit!");
        return resultat;
    }
}
